<?php require_once('publish.php'); ?>
<?php require_once('/var/www/.config/morris/config.php'); ?>
<?php
// Check token
if (!array_key_exists('HTTP_X_REBUILD_ME_TOKEN', $_SERVER) ||
  $_SERVER['HTTP_X_REBUILD_ME_TOKEN'] !== REBUILD_ME_TOKEN) {
  if (!array_key_exists('QUERY_STRING', $_SERVER) ||
    $_SERVER['QUERY_STRING'] !== REBUILD_ME_TOKEN) {
    header('HTTP/1.1 401 Unauthorized');
    echo "invalid_authorization - ", $_SERVER['HTTP_X_REBUILD_ME_TOKEN'];
    exit();
  }
}

if (array_key_exists('HTTP_X_REBUILD_ME_TOKEN', $_SERVER) &&
    $_SERVER['HTTP_X_REBUILD_ME_TOKEN'] == REBUILD_ME_TOKEN) {
    publish();
    echo "Rebuild OK\n";
} elseif (array_key_exists('QUERY_STRING', $_SERVER) &&
  $_SERVER['QUERY_STRING'] == REBUILD_ME_TOKEN) {
  publish();
  echo "Rebuild OK\n";
} else {
  echo "Something Wrong!";
  echo "invalid_authorization - ", $_SERVER['HTTP_X_REBUILD_ME_TOKEN'];
  exit();
}

?>
<?php if (empty($entityBody)): ?>
	<html><body>
	Hi. I'm a receiver for webhook from gitlab!
	</body></html>
<?php endif ?>
