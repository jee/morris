<?php require_once('publish.php'); ?>
<?php require_once('/var/www/.config/morris/config.php'); ?>
<?php
// Check token
if (!array_key_exists('HTTP_X_GITLAB_TOKEN', $_SERVER) ||
    $_SERVER['HTTP_X_GITLAB_TOKEN'] !== GITLAB_WEBHOOK_TOKEN) {
    header('HTTP/1.1 401 Unauthorized');
    echo "invalid_authorization - ", $_SERVER['HTTP_X_GITLAB_TOKEN'];
    exit();
}
if (array_key_exists('HTTP_X_GITLAB_TOKEN', $_SERVER) &&
    $_SERVER['HTTP_X_GITLAB_TOKEN'] == GITLAB_WEBHOOK_TOKEN) {
      $entityBody = file_get_contents("php://input");
      if (! empty($entityBody)){
	  $json = json_decode($entityBody, true);

	  $fp = fopen('/tmp/log_rebuild.txt', 'w');
	  fwrite($fp, print_r($json['builds'][0]['status'], TRUE));
	  fclose($fp);
	  $build_status = $json['builds'][0]['status'];
	    if ($build_status == 'success'){
		echo "Build Status is: ", $build_status, "\nRebuild OK\n";
		header("HTTP/1.1 200 OK");
		// to avoid timeout from gitlab trigger
		// see https://stackoverflow.com/questions/15273570/continue-processing-php-after-sending-http-response
		session_write_close(); //close the session
		fastcgi_finish_request(); //this returns 200 to the user, and processing continues
		publish();
		$log_entity_body = print_r($entityBody, true);
	    } else {
		echo "Build Status is: ", $build_status;
	    }
      }
}
?>
<?php if (empty($entityBody)): ?>
	<html><body>
	Hi. I'm a receiver for webhook from gitlab!
	</body></html>
<?php endif ?>
