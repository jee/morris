<?php
/**
* Send to staticman,.
*/
function staticman_webmention($data) {
  $json = json_decode($data, true);
  $post = $json['post'];
  $author = $post['author'];
  $content = $post['content'];
  $type_of = $post['wm-property'];
    if ($type_of == 'in-reply-to') {
        $type = "reply";
    } elseif ($type_of == 'like-of') {
      $type = "like";
    } elseif ($type_of == 'repost-of') {
      $type = "repost";
    } elseif ($type_of == 'rsvp') {
      $type = "reply";
    } elseif ($type_of == 'bookmark-of') {
      $type = "like";
    }

  if ($type == 'like'){
    $staticman_url = 'https://staticman-frama.herokuapp.com/v3/entry/gitlab/jee/webmentions/master/wm_likes?';
  } elseif ($type == 'reply'){
    $staticman_url = 'https://staticman-frama.herokuapp.com/v3/entry/gitlab/jee/webmentions/master/wm_comments?';
  } elseif ($type == 'repost'){
    $staticman_url = 'https://staticman-frama.herokuapp.com/v3/entry/gitlab/jee/webmentions/master/wm_repost?';
  }

  $slug = parse_url($json['target'], PHP_URL_PATH);
  $split_slug = preg_split('/\//', $slug);
  $target_section = $split_slug[1];
  $target_filename =  $split_slug[2];
  $wm_id = $json['wm-id'];
  $wm_source = $json['wm-source'];
  $source_url = $json['source'];
  $published = $json['published'];
  $wm_received = $json['wm-received'];
  $name = $author['name'];
  $photo = $author['photo'];
  $website = $author['url'];
  $comment = $content['html'];

  $data = array(
	'options[slug]' => $slug,
	'options[target_section]' => $target_section,
	'options[target_filename]' => $target_filename,
	'options[type]' => $type,
	'fields[type]' => $type,
	'fields[wm_id]' => $wm_id,
	'fields[wm_source]' => $wm_source,
	'fields[target_url]' => $slug,
	'fields[source_url]' => $source_url,
	'fields[received]' => $wm_received,
	'fields[published]' => $published,
	'fields[name]' => $name,
	'fields[website]' => $website,
	'fields[photo]' => $photo,
	'fields[comment]' => $comment,
  );
  
  $fields_string = http_build_query($data);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $staticman_url);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
  curl_setopt($ch, CURLOPT_TIMEOUT, 600);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  $output = json_decode(curl_exec($ch), true);
  $retry = 0;
  while($output['success'] != "true" && $retry < 10){
    sleep(20);
    $output = json_decode(curl_exec($ch), true);
    $info = curl_getinfo($ch);
    $retry++;
  }
  curl_close($ch);
  if ($output['success'] != "true" && $retry >= 10 {
    if (!empty($wm_id) {
      $failed_wm = '/var/www/morris/failed_wm/'.$wm_id;
    } else {
      $timestamp = time();
      $failed_wm = '/var/www/morris/failed_wm/'.$timestamp;
    }
    file_put_contents($failed_wm, $fields_string, FILE_APPEND);
    // DEBUG
    $log_data = print_r($data, true);
    $log_output = print_r($output, true);
    $log_info = print_r($info, true);
    file_put_contents('/tmp/staticman_php.log', $log_data, FILE_APPEND);
    file_put_contents('/tmp/staticman_php.log', $log_output, FILE_APPEND);
    file_put_contents('/tmp/staticman_php.log', $log_info, FILE_APPEND);
  }
}
