<?php require_once('models.php'); ?>
<?php require_once('publish.php'); ?>
<?php require_once('passthrough.php'); ?>
<?php require_once('staticman.php'); ?>
<?php require_once('/var/www/.config/morris/config.php'); ?>
<?php

$entityBody = file_get_contents("php://input");
if (! empty($entityBody)){
	$json = json_decode($entityBody, true);
	if ($json['secret'] && ($json['secret'] == WMIO_WEBHOOK_TOKEN)) {
		echo "Processing a mention!\n";
		/* I don't need write in hugo/data anymore.
		 * $post = $json['post'];
		 * $mention = new Webmention($post);
		 * $ws = new WebmentionStore();
		 * $ws->add_mention($mention);
		 * $wi->add_mention($mention);
		 * $wi->save();
		 * publish();
		 * $log_entity_body = print_r($entityBody, true);
		 */
		/*DEBUG*/
		file_put_contents('/tmp/staticman_php.log', $log_entity_body, FILE_APPEND);
		passthrough_webmention($entityBody);
		staticman_webmention($entityBody);
		publish();
		echo "OK\n";
	}
}
?>
<?php if (empty($entityBody)): ?>
	<html><body>
	Hi. I'm a receiver for mentions from webmention.io!
	</body></html>
<?php endif ?>
